/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.lababidy.institute;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.lababidy.database.Course;
import org.lababidy.database.CourseDAO;
import static org.lababidy.institute.MainApp.scene;
import static org.lababidy.institute.MainApp.stage;

/**
 * FXML Controller class
 *
 * @author Mohannad Lababidy (m.lababidy@gmail.com)
 */
public class CourseFX implements Initializable {
    @FXML
    private ListView<Course> courseList;
    @FXML
    private TextField courseNametf;
    @FXML
    private TextArea courseNotesta;
    @FXML
    private Button okbtn;
    @FXML
    private Button cancelbtn;

    /**
     * Initializes the controller class.
     */
    int id = 0;
    ObservableList<Course> courseList1 = FXCollections.observableArrayList();
    CourseDAO courseDao = new CourseDAO();
    @FXML
    private Label messageLabel;
    @FXML
    private Button clearFields;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        courseList1.addAll(courseDao.findAll());
        courseList.setItems(courseList1);

    }


    @FXML
    private void anAction(ActionEvent event) {

        Course course = new Course();
        course.setcid(id);
        course.setcname(courseNametf.getText());
        course.setcnotes(courseNotesta.getText());
        courseDao.save(course);
        courseList1.clear();
        courseList1.addAll(courseDao.findAll());
        courseList.setItems(courseList1);
        courseList.getSelectionModel().select(course);
        id = courseList.getSelectionModel().getSelectedItem().getcid();
        messageLabel.setText("Course: " + courseNametf.getText() + " Saved");
    }

    @FXML
    private void onCancel(ActionEvent event) throws Throwable {
        stage.setScene(scene);
        stage.show();
        CourseFX.this.finalize();
    }

    @FXML
    private void onMouseClickedCourseList(MouseEvent event) {
        if (!courseList.getItems().isEmpty()) {
            id = courseList.getSelectionModel().getSelectedItem().getcid();
            courseNametf.setText(courseList.getSelectionModel().getSelectedItem().getcname());
            courseNotesta.setText(courseList.getSelectionModel().getSelectedItem().getcnotes());

        }

    }

    @FXML
    private void onDeleteCourse(ActionEvent event) {
        if (courseDao.remove(id)) {
            messageLabel.setText("Course: " + courseNametf.getText() + " removed");
            courseList1.clear();
            courseList1.addAll(courseDao.findAll());
            courseList.setItems(courseList1);
            id = 0;
        }

    }

    @FXML
    private void onClearFields(ActionEvent event) {
        id = 0;
        courseNametf.setText(null);
        courseNotesta.setText(null);

    }

}
