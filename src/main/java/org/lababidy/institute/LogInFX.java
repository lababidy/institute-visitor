package org.lababidy.institute;

import org.lababidy.database.StaffDAO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableArrayBase;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.AnchorPane;
import static org.lababidy.institute.MainApp.logger;
import static org.lababidy.institute.MainApp.visitorScene;
import static org.lababidy.institute.MainApp.courseScene;
import static org.lababidy.institute.MainApp.stage;

public class LogInFX implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button okbtn;
    @FXML
    private ComboBox<String> ComboUserName;
    @FXML
    private PasswordField passwordtf;
    @FXML
    private Button addVisitorButton;
    @FXML
    private Button addCourseButton;

    @FXML
    private void handleButtonAction(ActionEvent event) {

        System.out.println("" + ComboUserName.getValue() + " " + passwordtf.getText() + "");
        label.setText("Hello World!");
        if (StaffDAO.login(ComboUserName.getValue(), passwordtf.getText())) {
            addCourseButton.setDisable(false);
            addVisitorButton.setDisable(false);

        };

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
addCourseButton.setDisable(true);
        addVisitorButton.setDisable(true);
        ObservableList<String> names = FXCollections.observableArrayList();
        //staffDAO staffdaw = new StaffDAO();
        names.addAll(StaffDAO.listNames());
        ComboUserName.setItems(names);
        //staffdaw=null;

    }    

    @FXML
    private void onActionAddVisitorButton(ActionEvent event) {
        stage.setScene(visitorScene);
        stage.setTitle("Visitor Info");
        logger.info("Login !stage");
        stage.show();

    }

    @FXML
    private void onActionAddCourseButton(ActionEvent event) {

        stage.setScene(courseScene);
        stage.setTitle("Visitor Info");
        logger.info("Login !stage");
        stage.show();
    }
}
