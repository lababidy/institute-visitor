package org.lababidy.institute;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.lababidy.database.Course;
import org.lababidy.database.CourseDAO;
import org.lababidy.database.Visitor;
import org.lababidy.database.VisitorDAO;
import static org.lababidy.institute.MainApp.logger;
import static org.lababidy.institute.MainApp.stage;
import static org.lababidy.institute.MainApp.scene;

/**
 * FXML Controller class
 *
 * @author Mohannad Lababidy (m.lababidy@gmail.com)
 */
public class VisitorFX implements Initializable {
    @FXML
    private TextField tbNotes;
    @FXML
    private TextField tbMobile;
    @FXML
    private TextField tbEmail;
    @FXML
    private TextField tbPhone;
    @FXML
    private TextField tbName;
    @FXML
    private TextField tblastname;
    @FXML
    private ComboBox<Course> CourseCombo;
    @FXML
    private Button okbtn;
    @FXML
    private Button cancelbtn;
    @FXML
    private ListView<Visitor> visitorsList;
    @FXML
    private Button ClearFieldsbtn;
    /**
     * Initializes the controller class.
     */
    @FXML
    private Button delVisitor;

    int id = 0, cid = 0;
    ObservableList<Course> courseList = FXCollections.observableArrayList();
    CourseDAO courseDao = new CourseDAO();

    ObservableList<Visitor> visitorsListco = FXCollections.observableArrayList();
    VisitorDAO visitorDao = new VisitorDAO();
    
    @FXML
    private Label messageLabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        courseList.addAll(courseDao.findAll());
        CourseCombo.setItems(courseList);
        
        visitorsListco.addAll(visitorDao.findAll());
        visitorsList.setItems(visitorsListco);
    }

    @FXML
    private void onVisitorCourseAction(ActionEvent event) {
        cid = CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcid();
    }
    @FXML
    private void onOkAction(ActionEvent event) {
        logger.info("getVid()" + id);
        //VisitorDAO visitorDao = new VisitorDAO();
        Visitor visitor = new Visitor();
        visitor.setVid(id);
        visitor.setVFname(tbName.getText());
        visitor.setVLname(tblastname.getText());
        visitor.setVEmail(tbEmail.getText());
        visitor.setVTelephone(tbPhone.getText());
        visitor.setVMobile(tbMobile.getText());
        visitor.setVcid(cid);
        logger.info("CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcid()"
                + CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcid());
        logger.info("cid" + cid);
        visitor.setvNotes(tbNotes.getText());
        visitorDao.save(visitor);
        visitorsListco.clear();
        visitorsListco.addAll(visitorDao.findAll());
        visitorsList.setItems(visitorsListco);
        visitorsList.getSelectionModel().select(visitor);
        id = visitorsList.getSelectionModel().getSelectedItem().getVid();
        messageLabel.setText("visitor: " + tbName.getText() + " " + tblastname.getText() + " Saved");
    }
    @FXML
    private void onCancelAction(ActionEvent event) throws Throwable {
        stage.setScene(scene);
        stage.show();
        VisitorFX.this.finalize();
    }
    @FXML
    private void OnMouseClickedVisitorsList(MouseEvent event) {
        if (!visitorsList.getItems().isEmpty()) {
            id = visitorsList.getSelectionModel().getSelectedItem().getVid();
            cid = visitorsList.getSelectionModel().getSelectedItem().getVcid();
            String NameOfCourSe = courseDao.findById(cid).getcname();
            ///System.out.println("" + courceDao.findById(cid).getcname());
            tbName.setText(visitorsList.getSelectionModel().getSelectedItem().getVFname());
            tblastname.setText(visitorsList.getSelectionModel().getSelectedItem().getVLname());
            tbEmail.setText(visitorsList.getSelectionModel().getSelectedItem().getVEmail());
            tbMobile.setText(visitorsList.getSelectionModel().getSelectedItem().getVMobile());
            tbNotes.setText(visitorsList.getSelectionModel().getSelectedItem().getNnotes());
            tbPhone.setText(visitorsList.getSelectionModel().getSelectedItem().getVTelephone());
            CourseCombo.selectionModelProperty().getValue().select(cid - 1);
            /*logger.info("visitorsList.getSelectionModel().getSelectedItem().getVcid()"                    
 secure            + visitorsList.getSelectionModel().getSelectedItem().getVcid() + "");
             logger.info("CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcid()"
                    + CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcid());
            logger.info("CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcname()"
                    + CourseCombo.selectionModelProperty().getValue().getSelectedItem().getcname());
            logger.info("");
             */
    }
    }

    @FXML
    private void onActionClearFields(ActionEvent event) {

        tbName.setText(null);
        tblastname.setText(null);
        tbEmail.setText(null);
        tbMobile.setText(null);
        tbNotes.setText(null);
        tbPhone.setText(null);
        CourseCombo.valueProperty().set(null);
        id = 0;

    }

    @FXML
    private void ondelVisitorAction(ActionEvent event) {

        if (visitorDao.remove(id)) {
            messageLabel.setText("visitor: " + tbName.getText() + " " + tblastname.getText() + " removed");
            visitorsListco.clear();
            visitorsListco.addAll(visitorDao.findAll());
            visitorsList.setItems(visitorsListco);
            id = 0;
        }
    }

}
