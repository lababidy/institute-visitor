package org.lababidy.institute;

import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainApp extends Application {

    static final Logger logger = Logger.getLogger(MainApp.class.getName());
    static Scene visitorScene, courseScene, scene;
    static Parent visitorRoot, courseRoot, root;
    static Stage stage;


    @Override
    public void start(Stage stage1) throws Exception {
        root = FXMLLoader.load(getClass().getResource("/fxml/LogIn.fxml"));
        
        scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        stage = stage1;
        //Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        visitorRoot = FXMLLoader.load(getClass().getResource("/fxml/Visitor.fxml"));
        visitorScene = new Scene(visitorRoot);

        courseRoot = FXMLLoader.load(getClass().getResource("/fxml/Course.fxml"));
        courseScene = new Scene(courseRoot);
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();

    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
