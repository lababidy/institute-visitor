
# README #

### What is this repository for? ###

* Institute Visitors Program is a part of institute program for taking notes from institute visitors 
it programmed in javaFX 8.05 and MySQL database it uses simple DAO implementation the program is for testing
* Version 0.7

### How do I get set up? ###

* use maven 3.1 to build the Project with java 8
setup MySQL add user institute with password institute add database file in resources folder institute.sql
or use src/main/resources/database.properties file to edit connection string i add SQLite sample database for testing.

* Mohannad lababidy m.lababidy@gmail.com